<?php
ini_set('default_charset','UTF-8');

/**
 * Nome objeto: ConexaoBD.php
 * Descrição: Classe para efetuar conexões com banco de dados
 * e executar procedures.
 * Data: 02/03/2016 11:34
 * Atividade: #2390 - @872
 */

class ConexaoBD
{

    // Propriedades
    private $Banco;
    private $Sentenca;
    private $Script;



    //Metodo para abrir conexão com o banco de dados
    private function AbrirConexao() {
        //Parametros de conexão com o banco de dados
        $urlBD = "mysql:host=127.0.0.1:3307";//"mysql:host=192.168.0.101";//"mysql:host=mysql.ucase.com.br";
        $NomeBD = "dbname=avaliacaotecnica";
        $UsuarioBD = "root";//"root";//
        $SenhaBD = "1234";

        try{
            //Instancia a propriedade $Banco com um novo objeto PDO passando os parametros de conexão com o banco de dados

            $this->Banco = new PDO("$urlBD;$NomeBD",$UsuarioBD,$SenhaBD,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));



            return true;

        }catch(PDOException $e){

            //Em casa de falha na conexão exibe o erro

            die("Não foi possível conectar ao banco de dados. " . $e->getMessage());


            return false; // Retorna FALSE se houve erro

        }



    }



    //Metodo para preparar a sentença a ser executada no banco de dados
    public function PrepararSentenca($_strSQL){

        //Aciona o método que abre a conexão;

        $this->AbrirConexao();

        //Prepara a sentença configurando a propriedade

        $this->Sentenca =  $this->Banco->prepare($_strSQL);

    }

    //Método para configurar um parametro de uma sentença
    public function ParametroSentenca($_parametro,$valor){

        $this->Sentenca->bindParam($_parametro,$valor);

    }

    //Função que executa o comando no banco de dados

    public function ExecutaComando($retorno = false){


        //Verifica se executou o comando no banco


        try {

            if($this->Sentenca->execute()){
                if($retorno == true){
                    return $this->Sentenca;

                }else {

                    return true;
                }
            }else{

                die("<br> Erro na sentenças:" . $this->Sentenca->errorInfo()[2]);

                //Se executou retorna TRUE
                print_r($this->Sentenca->errorinfo());

            }

        }catch(PDOException $r){
            die ("<br> Não foi possível executar comando no banco de dados: " . $r->getMessage()[0] );


        }

    }

    public function lastId(){
        try{
            return $this->Banco->lastInsertId();
        }
        catch(PDOException $r){
            die ("<br> Não foi possível executar comando no banco de dados: " . $r->getMessage() );
        }
    }






}