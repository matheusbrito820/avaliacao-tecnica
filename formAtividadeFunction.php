<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 12:44
 */


require_once 'clsAtividade.php';
require_once 'clsStatus.php';

if(isset($_POST['salvarAtividade'])){
    $atividade = new Atividade();
    $atividade->setNome($_POST['nome']);
    $atividade->setDescricao($_POST['descricao']);
    $atividade->setDtInicio($_POST['dtInicio']);
    $atividade->setDtFim($_POST['dtFim']);
    $atividade->setStatus($_POST['status']);
    $atividade->setSituacao($_POST['situacao']);
    $atividade->salvarAtividade($atividade);
}

if(isset($_POST['listarStatus'])){
    $status = new Status();
    $retorno = $status->listarStatus();
    echo '{';
    foreach($retorno as $key=>$obj){
        echo '"' . $obj->getId() . '":{"id":"' . $obj->getId() . '",';
        echo '"descricao":"' . $obj->getDescricao() .'"}';
        if($key < (count($retorno) - 1)){
            echo ',';
        }
    }
    echo '}';
}

if(isset($_POST['carregarAtividade'])){
    $atividade = new Atividade();
    $atividade->setId($_POST['id']);
    $retorno = $atividade->carregarAtividade($atividade);
    echo '{';
    foreach($retorno as $key=>$obj){
        echo '"' . $obj->getId() . '":{"id":"' . $obj->getId() . '",';
        echo '"status":"' . $obj->getStatus() .'",';
        echo '"nome":"' . $obj->getNome() . '",';
        echo '"descricao":"' . $obj->getDescricao() . '",';
        echo '"dtInicio":"' . $obj->getDtInicio() . '",';
        echo '"dtFim":"' . $obj->getDtFim() . '",';
        echo '"situacao":"' . $obj->getSituacao() . '"}';
        if($key < (count($retorno) - 1)){
            echo ',';
        }
    }
    echo '}';
}

if(isset($_POST['editarAtividade'])){
    $atividade = new Atividade();
    $atividade->setId($_POST['id']);
    $atividade->setNome($_POST['nome']);
    $atividade->setDescricao($_POST['descricao']);
    $atividade->setDtInicio($_POST['dtInicio']);
    $atividade->setDtFim($_POST['dtFim']);
    $atividade->setStatus($_POST['status']);
    $atividade->setSituacao($_POST['situacao']);
    $atividade->editarAtividade($atividade);
}