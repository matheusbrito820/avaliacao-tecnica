<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 12:14
 */


include_once 'clsAtividade.php';
include_once 'clsStatus.php';


//função para carregar o select
if(isset($_POST['listarStatus'])){
    //cria o objeto
    $status = new Status();
    $retorno = $status->listarStatus();
    //monta o json
    echo '{';
    foreach($retorno as $key=>$obj){
        echo '"' . $obj->getId() . '":{"id":"' . $obj->getId() . '",';
        echo '"descricao":"' . $obj->getDescricao() .'"}';
        if($key < (count($retorno) - 1)){
            echo ',';
        }
    }
    echo '}';
}

//função para carregar a tabela
if(isset($_POST['listarAtividades'])){
    //cria o objeto
    $atividade = new Atividade();
    $atividade->setSituacao($_POST['situacao']);
    $atividade->setStatus($_POST['status']);
    $retorno = $atividade->listarAtividades($atividade);
    //monta o json
    echo '{';
    foreach($retorno as $key=>$obj){
        echo '"' . $obj->getId() . '":{"id":"' . $obj->getId() . '",';
        echo '"status":"' . $obj->getStatus() .'",';
        echo '"nome":"' . $obj->getNome() . '",';
        echo '"descricao":"' . $obj->getDescricao() . '",';
        echo '"dtInicio":"' . $obj->getDtInicio() . '",';
        echo '"dtFim":"' . $obj->getDtFim() . '",';
        echo '"situacao":"' . $obj->getSituacao() . '"}';
        if($key < (count($retorno) - 1)){
            echo ',';
        }
    }
    echo '}';
}