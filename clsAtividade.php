<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 12:08
 */


require_once 'clsAtividadeBD.php';

class Atividade
{
    private $id;
    private $Status;
    private $nome;
    private $descricao;
    private $dtInicio;
    private $dtFim;
    private $situacao;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param mixed $Status
     */
    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getDtInicio()
    {
        return $this->dtInicio;
    }

    /**
     * @param mixed $dtInicio
     */
    public function setDtInicio($dtInicio)
    {
        $this->dtInicio = $dtInicio;
    }

    /**
     * @return mixed
     */
    public function getDtFim()
    {
        return $this->dtFim;
    }

    /**
     * @param mixed $dtFim
     */
    public function setDtFim($dtFim)
    {
        $this->dtFim = $dtFim;
    }

    /**
     * @return mixed
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param mixed $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    public function salvarAtividade(Atividade $_atividade){
        $atividadeBD = new AtividadeBD();
        return $atividadeBD->salvarAtividade($_atividade);
    }

    public function editarAtividade(Atividade $_atividade){
        $atividadeBD = new AtividadeBD();
        return $atividadeBD->editarAtividade($_atividade);
    }

    public function listarAtividades(Atividade $_atividade){
        $atividadeBD = new AtividadeBD();
        return $atividadeBD->listarAtividades($_atividade);
    }

    public function carregarAtividade(Atividade $_atividade){
        $atividadeBD = new AtividadeBD();
        return $atividadeBD->carregarAtividade($_atividade);
    }
}