<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 12:49
 */


require_once 'ConexaoBD.php';
require_once 'clsAtividade.php';

class AtividadeBD
{
    //função para incluir uma nova atividade
    public function salvarAtividade(Atividade $_atividade){
        $con = new ConexaoBD();

        $con->PrepararSentenca('CALL TB_ATIVIDADE_INSERIR(:status, :nome, :descricao, :dtInicio, :dtFim, :situacao)');
        $con->ParametroSentenca(':nome', $_atividade->getNome());
        $con->ParametroSentenca(':descricao', $_atividade->getDescricao());
        $con->ParametroSentenca(':dtInicio', $_atividade->getDtInicio());
        $con->ParametroSentenca(':dtFim', $_atividade->getDtFim());
        $con->ParametroSentenca(':status', $_atividade->getStatus());
        $con->ParametroSentenca(':situacao', $_atividade->getSituacao());

        $con->ExecutaComando();

        return true;
    }

    //função para editar uma atividade
    public function editarAtividade(Atividade $_atividade){
        $con = new ConexaoBD();

        $con->PrepararSentenca('CALL TB_ATIVIDADE_EDITAR(:id, :status, :nome, :descricao, :dtInicio, :dtFim, :situacao)');
        $con->ParametroSentenca(':id', $_atividade->getId());
        $con->ParametroSentenca(':nome', $_atividade->getNome());
        $con->ParametroSentenca(':descricao', $_atividade->getDescricao());
        $con->ParametroSentenca(':dtInicio', $_atividade->getDtInicio());
        $con->ParametroSentenca(':dtFim', $_atividade->getDtFim());
        $con->ParametroSentenca(':status', $_atividade->getStatus());
        $con->ParametroSentenca(':situacao', $_atividade->getSituacao());

        $con->ExecutaComando();

        return true;
    }

    //função para listar uma atividade
    public function listarAtividades(Atividade $_atividade){
        $con = new ConexaoBD();

        $con->PrepararSentenca('CALL TB_ATIVIDADE_LISTAR(:status, :situacao)');
        $con->ParametroSentenca(':status', $_atividade->getStatus());
        $con->ParametroSentenca(':situacao', $_atividade->getSituacao());

        $retorno = $con->ExecutaComando(true);

        $arrayAtividade = array();

        //se retornou pelo menos um elemento
        if($retorno->rowCount() >= 1){
            //enche o array com os objetos
            foreach($retorno as $obj){
                $atividade = new Atividade();
                $atividade->setId($obj['ID_ATIVIDADE']);
                $atividade->setStatus($obj['DE_STATUS']);
                $atividade->setNome($obj['DE_NOME']);
                $atividade->setDescricao($obj['DE_DESCRICAO']);
                $atividade->setDtInicio($obj['DT_INICIO']);
                $atividade->setDtFim($obj['DT_FIM']);
                $atividade->setSituacao($obj['SITUACAO']);
                array_push($arrayAtividade, $atividade);
            }
        }

        //retorna o array
        return $arrayAtividade;
    }

    //função para consultar uma atividade
    public function carregarAtividade(Atividade $_atividade){
        $con = new ConexaoBD();

        $con->PrepararSentenca('CALL TB_ATIVIDADE_CONSULTAR(:id)');
        $con->ParametroSentenca(':id', $_atividade->getId());

        $retorno = $con->ExecutaComando(true);

        $arrayAtividade = array();

        //se retornou pelo menos um elemento
        if($retorno->rowCount() >= 1){
            //enche o array com os objetos
            foreach($retorno as $obj){
                $atividade = new Atividade();
                $atividade->setId($obj['ID_ATIVIDADE']);
                $atividade->setStatus($obj['ID_STATUS']);
                $atividade->setNome($obj['DE_NOME']);
                $atividade->setDescricao($obj['DE_DESCRICAO']);
                $atividade->setDtInicio($obj['DT_INICIO']);
                $atividade->setDtFim($obj['DT_FIM']);
                $atividade->setSituacao($obj['ST_SITUACAO']);
                array_push($arrayAtividade, $atividade);
            }
        }

        //retorna o array
        return $arrayAtividade;
    }
}