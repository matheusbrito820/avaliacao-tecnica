<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 13:26
 */


require_once 'ConexaoBD.php';
require_once 'clsStatus.php';

class StatusBD
{
    //função para listar os status
    public function listarStatus(){
        $con = new ConexaoBD();

        $con->PrepararSentenca('CALL TB_STATUS_LISTAR()');

        $retorno = $con->ExecutaComando(true);

        $arrayStatus = array();

        //se retornou pelo menos um elemento
        if($retorno->rowCount() >= 1){
            //enche o array com os objetos
            foreach($retorno as $obj){
                $status = new Status();
                $status->setId($obj['ID_STATUS']);
                $status->setDescricao($obj['DE_STATUS']);
                array_push($arrayStatus, $status);
            }
        }

        //retorna o array
        return $arrayStatus;
    }
}