-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.19-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para avaliacaotecnica
DROP DATABASE IF EXISTS `avaliacaotecnica`;
CREATE DATABASE IF NOT EXISTS `avaliacaotecnica` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `avaliacaotecnica`;

-- Copiando estrutura para tabela avaliacaotecnica.tb_atividade
DROP TABLE IF EXISTS `tb_atividade`;
CREATE TABLE IF NOT EXISTS `tb_atividade` (
  `ID_ATIVIDADE` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_STATUS` int(10) unsigned NOT NULL,
  `DE_NOME` varchar(256) NOT NULL,
  `DE_DESCRICAO` varchar(601) NOT NULL,
  `DT_INICIO` date NOT NULL,
  `DT_FIM` date NOT NULL,
  `ST_SITUACAO` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`ID_ATIVIDADE`,`ID_STATUS`),
  UNIQUE KEY `ID_ATIVIDADE_UNIQUE` (`ID_ATIVIDADE`),
  KEY `fk_TB_ATIVIDADE_TB_STATUS_idx` (`ID_STATUS`),
  CONSTRAINT `fk_TB_ATIVIDADE_TB_STATUS` FOREIGN KEY (`ID_STATUS`) REFERENCES `tb_status` (`ID_STATUS`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela avaliacaotecnica.tb_atividade: ~0 rows (aproximadamente)
DELETE FROM `tb_atividade`;
/*!40000 ALTER TABLE `tb_atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_atividade` ENABLE KEYS */;

-- Copiando estrutura para tabela avaliacaotecnica.tb_status
DROP TABLE IF EXISTS `tb_status`;
CREATE TABLE IF NOT EXISTS `tb_status` (
  `ID_STATUS` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DE_STATUS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_STATUS`),
  UNIQUE KEY `ID_STATUS_UNIQUE` (`ID_STATUS`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela avaliacaotecnica.tb_status: ~4 rows (aproximadamente)
DELETE FROM `tb_status`;
/*!40000 ALTER TABLE `tb_status` DISABLE KEYS */;
INSERT INTO `tb_status` (`ID_STATUS`, `DE_STATUS`) VALUES
	(1, 'Pendente'),
	(2, 'Em Desenvolvimento'),
	(3, 'Em Teste'),
	(4, 'Concluído');
/*!40000 ALTER TABLE `tb_status` ENABLE KEYS */;

-- Copiando estrutura para procedure avaliacaotecnica.TB_ATIVIDADE_CONSULTAR
DROP PROCEDURE IF EXISTS `TB_ATIVIDADE_CONSULTAR`;
DELIMITER //
CREATE PROCEDURE `TB_ATIVIDADE_CONSULTAR`(
	IN `paramId` INT
)
BEGIN

SELECT * FROM TB_ATIVIDADE WHERE ID_ATIVIDADE = paramId;

END//
DELIMITER ;

-- Copiando estrutura para procedure avaliacaotecnica.TB_ATIVIDADE_EDITAR
DROP PROCEDURE IF EXISTS `TB_ATIVIDADE_EDITAR`;
DELIMITER //
CREATE PROCEDURE `TB_ATIVIDADE_EDITAR`(
	IN `paramId` INT,
	IN `paramStatus` INT,
	IN `paramNome` VARCHAR(256),
	IN `paramDescricao` VARCHAR(600),
	IN `paramDtInicio` DATE,
	IN `paramDtFim` DATE,
	IN `paramSituacao` INT
)
BEGIN

UPDATE TB_ATIVIDADE SET ID_STATUS = paramStatus, DE_NOME = paramNome, DE_DESCRICAO = paramDescricao, DT_INICIO = paramDtInicio, DT_FIM = paramDtFim, ST_SITUACAO = paramSituacao WHERE ID_ATIVIDADE = paramId;

END//
DELIMITER ;

-- Copiando estrutura para procedure avaliacaotecnica.TB_ATIVIDADE_INSERIR
DROP PROCEDURE IF EXISTS `TB_ATIVIDADE_INSERIR`;
DELIMITER //
CREATE PROCEDURE `TB_ATIVIDADE_INSERIR`(
	IN `paramStatus` INT,
	IN `paramNome` VARCHAR(256),
	IN `paramDescricao` VARCHAR(601),
	IN `paramDtInicio` DATE,
	IN `paramDtFim` DATE,
	IN `paramSituacao` TINYINT
)
BEGIN

INSERT INTO TB_ATIVIDADE SET ID_STATUS = paramStatus, DE_NOME = paramNome, DE_DESCRICAO = paramDescricao, DT_INICIO = paramDtInicio, DT_FIM = paramDtFim, ST_SITUACAO = paramSituacao;

END//
DELIMITER ;

-- Copiando estrutura para procedure avaliacaotecnica.TB_ATIVIDADE_LISTAR
DROP PROCEDURE IF EXISTS `TB_ATIVIDADE_LISTAR`;
DELIMITER //
CREATE PROCEDURE `TB_ATIVIDADE_LISTAR`(
	IN `paramStatus` INT,
	IN `paramSituacao` VARCHAR(50)


)
BEGIN

	DECLARE VCONDICAO VARCHAR(3000) DEFAULT '';
   
	IF paramStatus <> 0 THEN 
		SET VCONDICAO = CONCAT(VCONDICAO, ' AND ATI.ID_STATUS = ', paramStatus);
	END IF;
	
	IF paramSituacao <> '-' then
		SET VCONDICAO = CONCAT(VCONDICAO, ' AND ATI.ST_SITUACAO = ', paramSituacao);
	end if;

	set @STRSQL=CONCAT('SELECT ATI.ID_ATIVIDADE, ST.DE_STATUS, ATI.DE_NOME, ATI.DE_DESCRICAO, ATI.DT_INICIO, ATI.DT_FIM, IF(ATI.ST_SITUACAO = 0, "Ativo", "Inativo") AS SITUACAO
	FROM TB_ATIVIDADE ATI
	INNER JOIN TB_STATUS ST ON ST.ID_STATUS = ATI.ID_STATUS
	WHERE 1 ', VCONDICAO);

	PREPARE STM FROM @STRSQL;
	EXECUTE STM;
	DEALLOCATE PREPARE STM;

END//
DELIMITER ;

-- Copiando estrutura para procedure avaliacaotecnica.TB_STATUS_LISTAR
DROP PROCEDURE IF EXISTS `TB_STATUS_LISTAR`;
DELIMITER //
CREATE PROCEDURE `TB_STATUS_LISTAR`()
BEGIN

SELECT * FROM TB_STATUS;

END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
