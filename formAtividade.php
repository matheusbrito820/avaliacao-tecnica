<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 11:28
 */

?>

<html>
<head>
    <title>
        Incluir Atividade
    </title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=no">
    <style>
        /* following three (cascaded) are equivalent to above three meta viewport statements */
        /* see http://www.quirksmode.org/blog/archives/2014/05/html5_dev_conf.html */
        /* see http://dev.w3.org/csswg/css-device-adapt/ */
        @-ms-viewport { width: 100vw ; min-zoom: 100% ; zoom: 100% ; }          @viewport { width: 100vw ; min-zoom: 100% zoom: 100% ; }
        @-ms-viewport { user-zoom: fixed ; min-zoom: 100% ; }                   @viewport { user-zoom: fixed ; min-zoom: 100% ; }
        /*@-ms-viewport { user-zoom: zoom ; min-zoom: 100% ; max-zoom: 200% ; }   @viewport { user-zoom: zoom ; min-zoom: 100% ; max-zoom: 200% ; }*/
    </style>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" class="uib-framework-theme">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
    <script>
        //função para carregar o select do status
        function carregarSelect(){
            $.ajax({
                url: 'formAtividadeFunction.php',
                type: 'post',
                data: {
                    'listarStatus': true
                },
                success: function(response){
                    try{
                        //transforma a resposta
                        json = $.parseJSON(response);
                        //recupera o objeto que será inserido
                        obj = document.getElementById('selectStatus');
                        insert = '';
                        //para cada objeto
                        $.each(json, function(index, current){
                            //html do select
                            insert += '<option value="' + current['id'] + '">' + current['descricao'] + '</option>';
                        });
                        //insere no objeto
                        obj.innerHTML = insert;
                    }
                    //tratamento de exception
                    catch(e){
                        console.log(e);
                        alert(response);
                    }
                }
            });
        }

        //função para salvar a atividade
        function salvarAtividade(){
            //recupera os dados digitados pelo usuário
            nome = $('#nomeAtividade').val();
            descricao = $('#descricaoAtividade').val();
            dtInicio = $('#dtInicio').val();
            dtFim = $('#dtFim').val();
            status = $('#selectStatus').val();
            situacao = $('#selectSituacao').val();

            //trata os dados
            if(nome.length > 255){
                alert('Nome da atividade muito grande. Valor máximo permitido: 255 caracteres.');
                return false;
            }

            if(descricao.length > 600){
                alert('Descrição da atividade muito grande. Valor máximo permitido: 600 caracteres.');
                return false;
            }

            if(nome == ''){
                alert('Nome de preenchimento obrigatório.');
                return false;
            }

            if(descricao == ''){
                alert('Descrição de preenchimento obrigatório.');
                return false;
            }

            if(dtInicio == ''){
                alert('Data de início de preenchimento obrigatório.');
                return false;
            }

            if(status != 4){
                if(dtFim == ''){
                    alert('Data de fim de preenchimento obrigatório.');
                    return false;
                }
                if(dtInicio > dtFim){
                    alert('Data de início da atividade maior que a data de fim.');
                    return false;
                }
            }

            //realiza o ajax
            $.ajax({
                url: 'formAtividadeFunction.php',
                type: 'post',
                data: {
                    'salvarAtividade': true,
                    'nome': nome,
                    'descricao': descricao,
                    'dtInicio': dtInicio,
                    'dtFim': dtFim,
                    'status': status,
                    'situacao': situacao
                },
                success: function(response){
                    //se o usuário deseja incluir nova atividade limpa a tela
                    if(confirm('Atividade registrada com sucesso. Deseja incluir uma nova atividade?') == true){
                        $('#nomeAtividade').val('');
                        $('#descricaoAtividade').val('');
                        $('#dtInicio').val('');
                        $('#dtFim').val('');
                        $('#selectStatus').val('1');
                        $('#selectSituacao').val('0');
                    }
                    //senão volta pra lista
                    else{
                        window.location = 'listAtividades.php';
                    }
                }
            });
        }
    </script>
</head>
<body style="background-color: #F2F1EC;" onload="carregarSelect()">
<div class="container" align="center">
    <div class="container-fluid">
        <div class="row">
            <h1>Incluir Atividade</h1>
        </div>
        <div class="row">
            <div class="form-group col-xs-12 table-thing">
                <label class="narrow-control label-top-left">Nome</label>
                <input type="text" class="form-control" name="nomeAtividade" id="nomeAtividade">
            </div>
            <div class="form-group col-xs-12 table-thing">
                <label class="narrow-control label-top-left">Descrição</label>
                <textarea class="form-control" rows="3" name="descricaoAtividade" id="descricaoAtividade"></textarea>
            </div>
            <div class="form-group col-xs-6 table-thing">
                <label class="narrow-control label-top-left">Data de Início</label>
                <input type="date" class="form-control" name="dtInicio" id="dtInicio">
            </div>
            <div class="form-group col-xs-6 table-thing">
                <label class="narrow-control label-top-left">Data de Fim</label>
                <input type="date" class="form-control" name="dtFim" id="dtFim">
            </div>
            <div class="form-group col-xs-3 table-thing">
                <label class="narrow-control label-top-left">Status</label>
                <select class="form-control" name="selectStatus" id="selectStatus">

                </select>
            </div>
            <div class="form-group col-xs-3 table-thing">
                <label class="narrow-control label-top-left">Situação</label>
                <select class="form-control" name="selectSituacao" id="selectSituacao">
                    <option value="0" selected>Ativo</option>
                    <option value="1">Inativo</option>
                </select>
            </div>
            <div class="form-group col-xs-offset-3 col-xs-3">
                <button type="button" class="btn btn-success" onclick="salvarAtividade()" style="margin-top: 25px"><span class="glyphicon glyphicon-ok"></span> Salvar Atividade</button>
            </div>
        </div>
    </div>
</div>
</body>