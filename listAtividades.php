<?php
/**
 * Created by PhpStorm.
 * User: mathe
 * Date: 26/08/2017
 * Time: 11:28
 */

?>

<html>
<head>
    <title>
        Controle de Atividades
    </title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=no">
    <style>
        /* following three (cascaded) are equivalent to above three meta viewport statements */
        /* see http://www.quirksmode.org/blog/archives/2014/05/html5_dev_conf.html */
        /* see http://dev.w3.org/csswg/css-device-adapt/ */
        @-ms-viewport { width: 100vw ; min-zoom: 100% ; zoom: 100% ; }          @viewport { width: 100vw ; min-zoom: 100% zoom: 100% ; }
        @-ms-viewport { user-zoom: fixed ; min-zoom: 100% ; }                   @viewport { user-zoom: fixed ; min-zoom: 100% ; }
        /*@-ms-viewport { user-zoom: zoom ; min-zoom: 100% ; max-zoom: 200% ; }   @viewport { user-zoom: zoom ; min-zoom: 100% ; max-zoom: 200% ; }*/
    </style>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" class="uib-framework-theme">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
    <script type="application/javascript" src="js/moment.js"></script>
    <script>
        //função que carrega as atividades na tabela
        function carregarAtividades(){
            //recupera os valores nos filtros
            status = $('#selectStatus').val();
            situacao = $('#selectSituacao').val();
            //realiza o ajax
            $.ajax({
                url: 'listAtividadesFunction.php',
                type: 'post',
                data: {
                    'listarAtividades': true,
                    'status': status,
                    'situacao': situacao
                },
                success: function(response){
                    //pega o objeto a ser inserido
                    obj = document.getElementById('tbodyAtividade');
                    insert = '';
                    //se não tem atividades
                    if(response == '{}'){
                        insert += '<tr><td colspan="7"><h3 align="center">Não há atividades registradas</h3></td></tr>';
                    }
                    //se tem atividades
                    else{
                        try{
                            //converte o json
                            json = $.parseJSON(response);
                            //pra cada objeto
                            $.each(json, function(index, current){
                                //se for concluído deixa a linha verde
                                if(current['status'] == 'Concluído'){
                                    insert += '<tr class="success">';
                                }
                                //senão linha normal
                                else{
                                    insert += '<tr>';
                                }
                                //html da linha da atividade
                                insert +=
                                    '   <td>' + current['id'] + '</td>' +
                                    '   <td>' + current['nome'] + '</td>' +
                                    '   <td>' + current['descricao'] + '</td>' +
                                    '   <td>' + moment(current['dtInicio'], 'YYYY-MM-DD').format('DD/MM/YYYY') + '</td>' +
                                    '   <td>' + moment(current['dtFim'], 'YYYY-MM-DD').format('DD/MM/YYYY') + '</td>' +
                                    '   <td>' + current['status'] + '</td>' +
                                    '   <td>' + current['situacao'] + '</td>' +
                                    '   <td><button class="btn btn-primary" onclick="editarAtividade(' + current['id'] + ')"><span class="glyphicon glyphicon-pencil"></span></button></td>' +
                                    '</tr>';
                            });
                        }
                        //trabalho da exception
                        catch(e){
                            alert(response);
                            console.log(e);
                        }
                    }
                    //insere o html
                    obj.innerHTML = insert;
                }
            });
        }

        //função para criar uma nova atividade. só entra na página para inclusão.
        function novaAtividade(){
            window.location = 'formAtividade.php';
        }

        //função que carrega o select do status
        function carregarSelect(){
            //realiza o ajax
            $.ajax({
                url: 'listAtividadesFunction.php',
                type: 'post',
                data: {
                    'listarStatus': true
                },
                success: function(response){
                    try{
                        //converte a resposta
                        json = $.parseJSON(response);
                        //recupera o objeto a ser inserido
                        obj = document.getElementById('selectStatus');
                        insert = '';
                        //para cada objeto
                        $.each(json, function(index, current){
                            //html do select
                            insert += '<option value="' + current['id'] + '">' + current['descricao'] + '</option>';

                        });
                        //insere o html
                        obj.insertAdjacentHTML('beforeend', insert);
                    }
                    //tratamento de exception
                    catch(e){
                        console.log(e);
                        alert(response);
                    }
                }
            });

            //após carregar o select carrega as atividades
            carregarAtividades();
        }

        //função para editar uma atividade. manda o usuário para a tela de edição passando a id da atividade por GET
        function editarAtividade(id){
            window.location = 'formAtividadeEditar.php?id=' + id;
        }
    </script>
</head>
<body style="background-color: #F2F1EC;" onload="carregarSelect()">
    <div class="container" align="center">
        <div class="container-fluid">
            <div class="row">
                <h1>Lista de Atividades</h1>
            </div>
            <div class="row">
                <div class="form-group col-xs-4 table-thing">
                    <label class="narrow-control label-top-left">Status</label>
                    <select class="form-control" name="selectStatus" id="selectStatus">
                        <option value="0" selected>Selecione um Status</option>
                    </select>
                </div>
                <div class="form-group col-xs-4 table-thing">
                    <label class="narrow-control label-top-left">Situação</label>
                    <select class="form-control" name="selectSituacao" id="selectSituacao">
                        <option value="-" selected>Selecione uma Situação</option>
                        <option value="0">Ativo</option>
                        <option value="1">Inativo</option>
                    </select>
                </div>
                <div class="form-group col-xs-4 table-thing">
                    <button type="button" class="btn btn-default" onclick="carregarAtividades()" style="margin-top: 25px"><span class="glyphicon glyphicon-filter"></span> Filtrar</button>
                </div>
            </div>
            <div class="row">
                <table class="table table-responsive" id="tAtividade">
                    <thead id="theadAtividade">
                        <th>#</th>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Dt. Início</th>
                        <th>Dt. Fim</th>
                        <th>Status</th>
                        <th>Situação</th>
                    </thead>
                    <tbody id="tbodyAtividade">

                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="form-group col-xs-4 table-thing">
                    <button type="button" class="btn btn-info" name="btnNovaAtividade" id="btnNovaAtividade" onclick="novaAtividade()"><span class="glyphicon glyphicon-plus"></span> Incluir nova atividade</button>
                </div>
            </div>
        </div>
    </div>
</body>